import 'package:alice_get_connect/alice_get_connect.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pokemon/_core/constants/app_constant.dart';
import 'package:pokemon/_core/constants/app_flavor.dart';
import 'package:pokemon/_core/constants/app_page.dart';
import 'package:pokemon/_core/initial_binding.dart';
import 'package:pokemon/_core/translations/_app_translation.dart';
import 'package:pokemon/presentation/_core/app_color.dart';
import 'package:overlay_support/overlay_support.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late final AliceGetConnect alice;

  @override
  void initState() {
    //INIT ALICE
    alice = Get.put(
      AliceGetConnect(
          timeout: const Duration(
        milliseconds: AppConstant.kConnectionTimeout,
      )),
      permanent: true,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return OverlaySupport.global(
      child: GetMaterialApp(
        debugShowCheckedModeBanner: false,
        navigatorKey: alice.getNavigatorKey(),
        title: AppFlavor.instance.appName,
        theme: ThemeData(
          primarySwatch: AppColor.primary,
          scaffoldBackgroundColor: Colors.white,
          useMaterial3: false,
        ),
        initialBinding: InitialBinding(),
        getPages: AppPage.pages,
        translations: AppTranslations(),
        locale: const Locale('id', 'ID'),
      ),
    );
  }
}
