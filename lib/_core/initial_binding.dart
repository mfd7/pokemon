import 'package:get/get.dart';
import 'package:pokemon/data/data_binding.dart';
import 'package:pokemon/domain/domain_binding.dart';
import 'package:pokemon/presentation/presentation_binding.dart';

class InitialBinding extends Bindings {
  @override
  void dependencies() {
    DataBinding().dependencies();
    DomainBinding().dependencies();
    PresentationBinding().dependencies();
  }
}
