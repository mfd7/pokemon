import 'package:get/get.dart';
import 'package:pokemon/presentation/screens/detail/pokemon_detail_binding.dart';
import 'package:pokemon/presentation/screens/detail/screens/pokemon_detail_screen.dart';
import 'package:pokemon/presentation/screens/home/home_binding.dart';
import 'package:pokemon/presentation/screens/home/screens/home_screen.dart';
import 'package:pokemon/presentation/screens/splash/screens/splash_screen.dart';
import 'package:pokemon/presentation/screens/splash/splash_binding.dart';

class AppPage {
  static const splashScreen = '/';
  static const homeScreen = '/home';
  static const detailScreen = '/detail';

  static var pages = <GetPage>[
    //SPLASH
    GetPage(
      name: splashScreen,
      page: () => const SplashScreen(),
      binding: SplashBinding(),
    ),
    //HOME
    GetPage(
      name: homeScreen,
      page: () => HomeScreen(),
      binding: HomeBinding(),
    ),
    //PROFILE
    GetPage(
      name: detailScreen,
      page: () => PokemonDetailScreen(),
      binding: PokemonDetailBinding(),
    ),
  ];
}
