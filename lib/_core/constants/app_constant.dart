class AppConstant {
  static const kAppName = 'e-Presensi BBWS Citarum';
  static const kToken = "TOKEN";
  static const kUser = "USER";
  static const kUsername = "USERNAME";
  static const kParsingDataCode = "parsing_data_exception";
  static const kConnectionError = "connection_timeout";

  static const kConnectionTimeout = 30000;
  static const kDefaultPadding = 16.0;
}
