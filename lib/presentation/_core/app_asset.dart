class AppIcon {
  static const icGrass94 = 'assets/icons/ic_grass_94.png';
  static const icAqua94 = 'assets/icons/ic_aqua_94.png';
  static const icDragon94 = 'assets/icons/ic_dragon_94.png';
  static const icElectric94 = 'assets/icons/ic_electric_94.png';
  static const icFairy94 = 'assets/icons/ic_fairy_94.png';
  static const icFight94 = 'assets/icons/ic_fight_94.png';
  static const icFire94 = 'assets/icons/ic_fire_94.png';
  static const icFlying94 = 'assets/icons/ic_flying_94.png';
  static const icGhost94 = 'assets/icons/ic_ghost_94.png';
  static const icGround94 = 'assets/icons/ic_ground_94.png';
  static const icInsect94 = 'assets/icons/ic_insect_94.png';
  static const icMetal94 = 'assets/icons/ic_metal_94.png';
  static const icNocturnal94 = 'assets/icons/ic_nocturnal_94.png';
  static const icNormal94 = 'assets/icons/ic_normal_94.png';
  static const icPoison94 = 'assets/icons/ic_poison_94.png';
  static const icPsychic94 = 'assets/icons/ic_psychic_94.png';
  static const icRock94 = 'assets/icons/ic_rock_94.png';
  static const icIce94 = 'assets/icons/ic_ice_94.png';
  static const icGrass20 = 'assets/icons/ic_grass_20.png';
  static const icAqua20 = 'assets/icons/ic_aqua_20.png';
  static const icDragon20 = 'assets/icons/ic_dragon_20.png';
  static const icElectric20 = 'assets/icons/ic_electric_20.png';
  static const icFairy20 = 'assets/icons/ic_fairy_20.png';
  static const icFight20 = 'assets/icons/ic_fight_20.png';
  static const icFire20 = 'assets/icons/ic_fire_20.png';
  static const icFlying20 = 'assets/icons/ic_flying_20.png';
  static const icGhost20 = 'assets/icons/ic_ghost_20.png';
  static const icGround20 = 'assets/icons/ic_ground_20.png';
  static const icInsect20 = 'assets/icons/ic_insect_20.png';
  static const icMetal20 = 'assets/icons/ic_metal_20.png';
  static const icNocturnal20 = 'assets/icons/ic_nocturnal_20.png';
  static const icNormal20 = 'assets/icons/ic_normal_20.png';
  static const icPoison20 = 'assets/icons/ic_poison_20.png';
  static const icPsychic20 = 'assets/icons/ic_psychic_20.png';
  static const icRock20 = 'assets/icons/ic_rock_20.png';
  static const icIce20 = 'assets/icons/ic_ice_20.png';
}

class AppImage {
  static const imgBbwsCitarum = 'assets/images/img_bbws_citarum.png';
  static const imgNotFound = 'assets/images/img_not_found.png';
  static const imgEmptyState = 'assets/images/img_empty_state.png';
  static const imgOnboarding = 'assets/images/img_onboarding.png';
}
