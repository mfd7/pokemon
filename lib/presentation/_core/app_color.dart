import 'package:flutter/material.dart';

class AppColor {
  static const primary = MaterialColor(
    0xFF26bef0,
    <int, Color>{
      50: primary50,
      100: primary100,
      200: primary200,
      300: primary300,
      400: primary400,
      500: primary500,
      600: primary600,
      700: primary700,
      800: primary800,
      900: primary900,
    },
  );

  //PRIMARY
  static const primary50 = Color(0xFFE9EBF8);
  static const primary100 = Color(0xFFC1C6D4);
  static const primary200 = Color(0xFFA3AABF);
  static const primary300 = Color(0xFF7984A2);
  static const primary400 = Color(0xFF5F6C90);
  static const primary500 = Color(0xFF374774);
  static const primary600 = Color(0xFF32416A);
  static const primary700 = Color(0xFF273252);
  static const primary800 = Color(0xFF1E2740);
  static const primary900 = Color(0xFF171E31);
  static const primaryLinear = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [
      primary500,
      Color(0xFF4464AD),
    ],
  );
  static const primaryDarkLinear = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [
      Color(0xFF1D67B6),
      Color(0xFF213A73),
    ],
  );

  //SECONDARY
  static const secondaryBase = Color(0xFFFFF8D3);
  static const secondaryNormal = Color(0xFFF2C85A);
  static const secondaryPressed = Color(0xFFDAB451);
  static const secondaryDark = Color(0xFF382C09);

  //NEUTRAL
  static const neutral1 = Color(0xFFffffff);
  static const neutral2 = Color(0xFFfcfcfc);
  static const neutral3 = Color(0xFFf5f5f5);
  static const neutral4 = Color(0xFFf0f0f0);
  static const neutral5 = Color(0xFFd9d9d9);
  static const neutral6 = Color(0xFFbfbfbf);
  static const neutral7 = Color(0xFF8c8c8c);
  static const neutral8 = Color(0xFF595959);
  static const neutral9 = Color(0xFF454545);
  static const neutral10 = Color(0xFF262626);
  static const neutral12 = Color(0xFF141414);
  static const neutral13 = Color(0xFF000000);

  //WARNING
  static const warningBase = Color(0xFFFEFAEA);
  static const warningNormal = Color(0xFFEFB734);
  static const warningDark = Color(0xFF382204);

  //INFO
  static const infoBase = Color(0xFFEFFEFA);
  static const infoNormal = Color(0xFF02E7F7);
  static const infoDark = Color(0xFF002238);

  //DANGER
  static const dangerBase = Color(0xFFFFF4EB);
  static const dangerNormal = Color(0xFFFF4326);
  static const dangerDark = Color(0xFF38030D);

  //SUCCESS
  static const successBase = Color(0xFFF9FBF1);
  static const successNormal = Color(0xFF8CBF16);
  static const successDark = Color(0xFF213702);

  //TEXT
  static const bodyText = Color(0xFF4B4B4B);

  //OUTLINE
  static const baseOutline = Color(0xFFE0E0E0);

  static const midnightBlue = Color(0xFF000029);
  static const aquaHaze = Color(0xFFEDF6EC);
  static const fern = Color(0xFF63BC5A);
  static const pastelOrange = Color(0xFFFF9D55);
  static const havelockBlue = Color(0xFF5090D6);
  static const blueEyes = Color(0xFF0B6DC3);
  static const sandstorm = Color(0xFFF4D23C);
  static const lightFuchsiaPink = Color(0xFFEC8FE6);
  static const waikawaGrey = Color(0xFF5269AD);
  static const pea = Color(0xFF91C12F);
  static const deepRose = Color(0xFFCE416B);
  static const downy = Color(0xFF73CEC0);
  static const mountainMist = Color(0xFF919AA2);
  static const davyGrey = Color(0xFF5A5465);
  static const horizon = Color(0xFF5A8EA2);
  static const scorrelBrown = Color(0xFFC5B78C);
  static const watermelonPink = Color(0xFFFA7179);
  static const rawSienna = Color(0xFFD97845);
  static const richLilac = Color(0xFFB567CE);
  static const glacier = Color(0xFF89AAE3);
  static const forgetMeNot = Color(0xFFFCF3EB);
  static const porcelain = Color(0xFFEBF1F8);
  static const ecruWhite = Color(0xFFF1F6E8);
  static const rumSwizzle = Color(0xFFFBF8E9);
  static const lavenderBlush = Color(0xFFFBF1FA);
  static const linen = Color(0xFFF9EFEA);
  static const springWood = Color(0xFFF7F5F1);
  static const porcelain2 = Color(0xFFF1F2F3);
  static const titanWhite = Color(0xFFF5EDF8);
  static const pearl = Color(0xFFFCEEEF);
  static const bluishCyan = Color(0xFFECF1F3);
  static const iceberg = Color(0xFFE4EEF6);
  static const amour = Color(0xFFF8E9EE);
  static const dawnPink = Color(0xFFECEBED);
  static const softPeach = Color(0xFFEBEDF4);
  static const blackSqueeze = Color(0xFFF1FBF9);
  static const catskillWhite = Color(0xFFF1F4FA);
}
