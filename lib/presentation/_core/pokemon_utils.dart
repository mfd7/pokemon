import 'package:flutter/material.dart';
import 'package:pokemon/presentation/_core/app_asset.dart';
import 'package:pokemon/presentation/_core/app_color.dart';

class PokemonUtils {
  static String getImage(String type) {
    String image;
    switch (type) {
      case 'normal':
        image = AppIcon.icNormal94;
      case 'fighting':
        image = AppIcon.icFight94;
      case 'flying':
        image = AppIcon.icFlying94;
      case 'poison':
        image = AppIcon.icPoison94;
      case 'ground':
        image = AppIcon.icGround94;
      case 'rock':
        image = AppIcon.icRock94;
      case 'bug':
        image = AppIcon.icInsect94;
      case 'ghost':
        image = AppIcon.icGhost94;
      case 'steel':
        image = AppIcon.icMetal94;
      case 'fire':
        image = AppIcon.icFire94;
      case 'water':
        image = AppIcon.icAqua94;
      case 'grass':
        image = AppIcon.icGrass94;
      case 'electric':
        image = AppIcon.icElectric94;
      case 'psychic':
        image = AppIcon.icPsychic94;
      case 'dragon':
        image = AppIcon.icDragon94;
      case 'fairy':
        image = AppIcon.icFairy94;
      case 'ice':
        image = AppIcon.icIce94;
      case 'dark':
        image = AppIcon.icNocturnal94;
      default:
        image = AppIcon.icGrass94;
    }
    return image;
  }

  static String getIcon(String type) {
    String image;
    switch (type) {
      case 'normal':
        image = AppIcon.icNormal20;
      case 'fighting':
        image = AppIcon.icFight20;
      case 'flying':
        image = AppIcon.icFlying20;
      case 'poison':
        image = AppIcon.icPoison20;
      case 'ground':
        image = AppIcon.icGround20;
      case 'rock':
        image = AppIcon.icRock20;
      case 'bug':
        image = AppIcon.icInsect20;
      case 'ghost':
        image = AppIcon.icGhost20;
      case 'steel':
        image = AppIcon.icMetal20;
      case 'fire':
        image = AppIcon.icFire20;
      case 'water':
        image = AppIcon.icAqua20;
      case 'grass':
        image = AppIcon.icGrass20;
      case 'electric':
        image = AppIcon.icElectric20;
      case 'psychic':
        image = AppIcon.icPsychic20;
      case 'dragon':
        image = AppIcon.icDragon20;
      case 'fairy':
        image = AppIcon.icFairy20;
      case 'ice':
        image = AppIcon.icIce20;
      case 'dark':
        image = AppIcon.icNocturnal20;
      default:
        image = AppIcon.icGrass20;
    }
    return image;
  }

  static Color getColor(String type) {
    Color color;
    switch (type) {
      case 'normal':
        color = AppColor.mountainMist;
      case 'fighting':
        color = AppColor.deepRose;
      case 'flying':
        color = AppColor.glacier;
      case 'poison':
        color = AppColor.richLilac;
      case 'ground':
        color = AppColor.rawSienna;
      case 'rock':
        color = AppColor.scorrelBrown;
      case 'bug':
        color = AppColor.pea;
      case 'ghost':
        color = AppColor.waikawaGrey;
      case 'steel':
        color = AppColor.horizon;
      case 'fire':
        color = AppColor.pastelOrange;
      case 'water':
        color = AppColor.havelockBlue;
      case 'grass':
        color = AppColor.fern;
      case 'electric':
        color = AppColor.sandstorm;
      case 'psychic':
        color = AppColor.watermelonPink;
      case 'dragon':
        color = AppColor.blueEyes;
      case 'fairy':
        color = AppColor.lightFuchsiaPink;
      case 'ice':
        color = AppColor.downy;
      case 'dark':
        color = AppColor.davyGrey;
      default:
        color = AppColor.fern;
    }
    return color;
  }

  static Color getBackgroundColor(String type) {
    Color color;
    switch (type) {
      case 'normal':
        color = AppColor.porcelain2;
      case 'fighting':
        color = AppColor.amour;
      case 'flying':
        color = AppColor.catskillWhite;
      case 'poison':
        color = AppColor.titanWhite;
      case 'ground':
        color = AppColor.linen;
      case 'rock':
        color = AppColor.springWood;
      case 'bug':
        color = AppColor.ecruWhite;
      case 'ghost':
        color = AppColor.softPeach;
      case 'steel':
        color = AppColor.bluishCyan;
      case 'fire':
        color = AppColor.forgetMeNot;
      case 'water':
        color = AppColor.porcelain;
      case 'grass':
        color = AppColor.aquaHaze;
      case 'electric':
        color = AppColor.rumSwizzle;
      case 'psychic':
        color = AppColor.pearl;
      case 'dragon':
        color = AppColor.iceberg;
      case 'fairy':
        color = AppColor.lavenderBlush;
      case 'ice':
        color = AppColor.blackSqueeze;
      case 'dark':
        color = AppColor.dawnPink;
      default:
        color = AppColor.aquaHaze;
    }
    return color;
  }
}
