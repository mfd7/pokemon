import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokemon/_core/constants/app_constant.dart';
import 'package:pokemon/presentation/_core/pokemon_utils.dart';
import 'package:pokemon/presentation/_core/app_asset.dart';
import 'package:pokemon/presentation/_core/app_text_style.dart';

class PokemonTypeChip extends StatelessWidget {
  final String type;
  final bool isLarge;
  const PokemonTypeChip({
    super.key,
    required this.type,
    this.isLarge = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: isLarge
            ? AppConstant.kDefaultPadding / 2
            : AppConstant.kDefaultPadding / 8 * 3,
        vertical: AppConstant.kDefaultPadding / 16 * 3,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: PokemonUtils.getColor(type),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            PokemonUtils.getIcon(type),
            scale: isLarge ? 0.6 : 1,
          ),
          SizedBox(
            width: isLarge
                ? AppConstant.kDefaultPadding / 4
                : AppConstant.kDefaultPadding / 8,
          ),
          Text(
            type.capitalize ?? type,
            style: isLarge ? AppTextStyle.titleSmall : AppTextStyle.labelSmall,
          )
        ],
      ),
    );
  }
}
