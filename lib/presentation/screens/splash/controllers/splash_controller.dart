import 'package:get/get.dart';
import 'package:pokemon/_core/constants/app_page.dart';

class SplashController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    Future.delayed(const Duration(seconds: 2), () {
      Get.offAllNamed(AppPage.homeScreen);
    });
  }
}
