import 'package:flutter/material.dart';
import 'package:pokemon/presentation/_core/app_color.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppColor.midnightBlue,
      ),
    );
  }
}
