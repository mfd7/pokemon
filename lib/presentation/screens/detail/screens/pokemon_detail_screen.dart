import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokemon/_core/constants/app_constant.dart';
import 'package:pokemon/presentation/_core/app_color.dart';
import 'package:pokemon/presentation/_core/app_text_style.dart';
import 'package:pokemon/presentation/_core/pokemon_utils.dart';
import 'package:pokemon/presentation/screens/detail/controllers/pokemon_detail_controller.dart';
import 'package:pokemon/presentation/_core/widgets/pokemon_type_chip.dart';

class PokemonDetailScreen extends StatelessWidget {
  final controller = Get.find<PokemonDetailController>();
  PokemonDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Positioned(
                top: -MediaQuery.of(context).size.width * 50 / 100,
                left: -MediaQuery.of(context).size.width * 10 / 100,
                child: Container(
                  width: MediaQuery.of(context).size.width +
                      MediaQuery.of(context).size.width * 20 / 100,
                  height: MediaQuery.of(context).size.width +
                      MediaQuery.of(context).size.width * 20 / 100,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: PokemonUtils.getColor(
                      controller.pokemonDetail.value.types.first,
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 0,
                right: 0,
                top: -50,
                child: SizedBox(
                  child: Image.asset(
                    scale: 0.5,
                    PokemonUtils.getImage(
                        controller.pokemonDetail.value.types.first),
                  ),
                ),
              ),
              Positioned(
                left: 10,
                top: 10,
                child: SafeArea(
                  child: InkWell(
                    onTap: () {
                      Get.back();
                    },
                    child: Container(
                      padding:
                          const EdgeInsets.all(AppConstant.kDefaultPadding / 2),
                      child: const Icon(
                        Icons.arrow_back_ios,
                        color: AppColor.neutral1,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: double.infinity,
                child: Column(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.width * 40 / 100,
                    ),
                    Obx(
                      () => SizedBox(
                        child: CachedNetworkImage(
                          imageUrl: controller.pokemonDetail.value.imageUrl,
                          height: 150,
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          Expanded(
            child: SingleChildScrollView(
              padding: const EdgeInsets.symmetric(
                  horizontal: AppConstant.kDefaultPadding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    controller.pokemonDetail.value.name.capitalize ??
                        controller.pokemonDetail.value.name,
                    style: AppTextStyle.headlineLarge,
                  ),
                  const SizedBox(
                    height: AppConstant.kDefaultPadding / 2 * 3,
                  ),
                  Row(
                    children: controller.pokemonDetail.value.types
                        .map(
                          (e) => Padding(
                            padding: const EdgeInsets.only(
                                right: AppConstant.kDefaultPadding / 2),
                            child: PokemonTypeChip(
                              type: e,
                              isLarge: true,
                            ),
                          ),
                        )
                        .toList(),
                  ),
                  const SizedBox(
                    height: AppConstant.kDefaultPadding / 2 * 3,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
