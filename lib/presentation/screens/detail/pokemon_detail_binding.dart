import 'package:get/get.dart';
import 'package:pokemon/presentation/screens/detail/controllers/pokemon_detail_controller.dart';

class PokemonDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(PokemonDetailController());
  }
}
