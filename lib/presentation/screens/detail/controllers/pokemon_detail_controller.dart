import 'package:get/get.dart';
import 'package:pokemon/domain/pokemon/entities/pokemon_detail.dart';

class PokemonDetailController extends GetxController {
  final pokemonDetail = PokemonDetail().obs;
  @override
  void onInit() {
    super.onInit();
    pokemonDetail.value = Get.arguments;
  }
}
