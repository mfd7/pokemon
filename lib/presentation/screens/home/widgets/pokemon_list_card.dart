import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokemon/_core/constants/app_constant.dart';
import 'package:pokemon/presentation/_core/pokemon_utils.dart';
import 'package:pokemon/domain/pokemon/entities/pokemon_detail.dart';
import 'package:pokemon/presentation/_core/app_text_style.dart';
import 'package:pokemon/presentation/_core/widgets/pokemon_type_chip.dart';

class PokemonListCard extends StatelessWidget {
  final PokemonDetail pokemon;
  final Function() onTap;
  const PokemonListCard({
    super.key,
    required this.pokemon,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: PokemonUtils.getBackgroundColor(pokemon.types.first),
        ),
        child: Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: AppConstant.kDefaultPadding,
                  top: AppConstant.kDefaultPadding / 4 * 3,
                  bottom: AppConstant.kDefaultPadding / 4 * 3,
                  right: AppConstant.kDefaultPadding / 8 * 5,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      pokemon.name.capitalize ?? pokemon.name,
                      style: AppTextStyle.titleLarge,
                    ),
                    const SizedBox(
                      height: AppConstant.kDefaultPadding / 4,
                    ),
                    Row(
                      children: pokemon.types
                          .map(
                            (e) => Padding(
                              padding: const EdgeInsets.only(
                                  right: AppConstant.kDefaultPadding / 4),
                              child: PokemonTypeChip(
                                type: e,
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(
                horizontal: AppConstant.kDefaultPadding,
                vertical: AppConstant.kDefaultPadding / 4,
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: PokemonUtils.getColor(pokemon.types.first),
              ),
              child: Stack(
                children: [
                  Image.asset(PokemonUtils.getImage(pokemon.types.first)),
                  Positioned(
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    child: CachedNetworkImage(
                      imageUrl: pokemon.imageUrl,
                      progressIndicatorBuilder: (context, _, __) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
