import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:pokemon/_core/constants/app_page.dart';
import 'package:pokemon/domain/pokemon/entities/pokemon_detail.dart';
import 'package:pokemon/domain/pokemon/repositories/pokemon_repository.dart';

class HomeController extends GetxController {
  final PokemonRepository _pokemonRepository;

  HomeController(this._pokemonRepository);

  final pokemonListDetail = <PokemonDetail>[].obs;
  final isLoading = false.obs;
  final isLoadingMore = false.obs;
  int offset = 10;
  final scrollController = ScrollController();

  @override
  void onInit() {
    super.onInit();
    retrievePokemonList();
    scrollController.addListener(_loadMorePokemon);
  }

  @override
  void onClose() {
    scrollController.removeListener(_loadMorePokemon);
    scrollController.dispose();
    super.onClose();
  }

  Future<void> retrievePokemonList({bool isLoadMore = false}) async {
    if (isLoadMore) {
      offset += 10;
    } else {
      isLoading(true);
    }
    final retrieve = await _pokemonRepository.retrievePokemonList(offset);
    retrieve.fold(
      (failure) {},
      (data) async {
        if (isLoadMore) {
          pokemonListDetail.addAll(data);
          pokemonListDetail.refresh();
        } else {
          pokemonListDetail.value = data;
        }
      },
    );
    isLoading(false);
  }

  _loadMorePokemon() {
    if (scrollController.position.pixels ==
            scrollController.position.maxScrollExtent &&
        !isLoadingMore.value) {
      retrievePokemonList(isLoadMore: true);
    }
  }

  navigateToDetail(PokemonDetail pokemonDetail) {
    Get.toNamed(AppPage.detailScreen, arguments: pokemonDetail);
  }
}
