import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokemon/_core/constants/app_constant.dart';
import 'package:pokemon/presentation/_core/app_color.dart';
import 'package:pokemon/presentation/_core/app_text_style.dart';
import 'package:pokemon/presentation/screens/home/controllers/home_controller.dart';
import 'package:pokemon/presentation/screens/home/widgets/pokemon_list_card.dart';

class HomeScreen extends StatelessWidget {
  final controller = Get.find<HomeController>();
  HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.neutral1,
      appBar: AppBar(
        backgroundColor: AppColor.midnightBlue,
        title: Text(
          'Pokemon',
          style: AppTextStyle.titleLarge.copyWith(color: AppColor.neutral1),
        ),
      ),
      body: SafeArea(
        child: Obx(
          () {
            if (controller.isLoading.isFalse) {
              return ListView.separated(
                controller: controller.scrollController,
                padding: const EdgeInsets.all(AppConstant.kDefaultPadding),
                itemBuilder: (context, index) {
                  if (index == controller.pokemonListDetail.length) {
                    return const Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: AppConstant.kDefaultPadding),
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }
                  return PokemonListCard(
                    pokemon: controller.pokemonListDetail.elementAt(index),
                    onTap: () => controller.navigateToDetail(
                      controller.pokemonListDetail.elementAt(index),
                    ),
                  );
                },
                separatorBuilder: (c, i) {
                  return const SizedBox(
                    height: AppConstant.kDefaultPadding / 4 * 3,
                  );
                },
                itemCount: controller.pokemonListDetail.length + 1,
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
