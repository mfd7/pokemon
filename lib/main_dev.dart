import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:pokemon/_core/constants/app_flavor.dart';
import 'package:pokemon/_core/my_app.dart';

void main() async {
  await GetStorage.init();
  await initializeDateFormatting("id_ID", null);
  Intl.defaultLocale = 'id_ID';
  WidgetsFlutterBinding.ensureInitialized();
  AppFlavor(
    baseUrl: 'https://pokeapi.co/api/v2',
    flavor: Flavor.dev,
  );
  runApp(const MyApp());
}
