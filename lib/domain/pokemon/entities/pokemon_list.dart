import 'package:pokemon/domain/pokemon/entities/pokemon.dart';

class PokemonList {
  final String next;
  final List<Pokemon> pokemonList;

  PokemonList({this.next = '', this.pokemonList = const []});
}
