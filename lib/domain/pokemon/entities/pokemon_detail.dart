class PokemonDetail {
  final String name;
  final String imageUrl;
  final List<String> types;

  PokemonDetail({this.name = '', this.imageUrl = '', this.types = const []});
}
