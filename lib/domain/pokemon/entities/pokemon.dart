class Pokemon {
  final String name;
  final String detailLink;

  Pokemon({this.name = '', this.detailLink = ''});
}
