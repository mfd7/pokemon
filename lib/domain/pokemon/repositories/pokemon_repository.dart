import 'package:dartz/dartz.dart';
import 'package:pokemon/domain/_core/app_failures.dart';
import 'package:pokemon/domain/pokemon/entities/pokemon_detail.dart';

abstract class PokemonRepository {
  Future<Either<AppFailure, List<PokemonDetail>>> retrievePokemonList(
      int offset);
}
