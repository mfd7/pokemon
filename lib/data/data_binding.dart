import 'package:alice_get_connect/alice_get_connect.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:pokemon/_core/constants/app_flavor.dart';
import 'package:pokemon/data/_core/api_base_helper.dart';
import 'package:pokemon/data/_core/interceptors/auth_interceptor.dart';
import 'package:pokemon/data/_core/interceptors/logger_interceptor.dart';
import 'package:pokemon/data/pokemon/data_pokemon_binding.dart';

class DataBinding extends Bindings {
  @override
  void dependencies() {
    //GET STORAGE
    Get.put(GetStorage(), permanent: true);
    //API BASE HELPER
    Get.put(
      ApiBaseHelper(
        client: GetConnect().httpClient,
        interceptors: [
          AuthInterceptor(Get.find()),
          LoggerInterceptor(),
          if (AppFlavor.isDev) ...[
            Get.find<AliceGetConnect>(),
          ],
        ],
      ),
      permanent: true,
    );

    //POKEMON
    DataPokemonBinding().dependencies();

    // //LAPORAN
    // DataLaporanBinding().dependencies();
    //
    // //PENGAJUAN
    // DataPengajuanBinding().dependencies();
    //
    // //PRESENSI
    // DataPresensiBinding().dependencies();
    //
    // //PROFILE
    // DataProfileBinding().dependencies();
    //
    // //PEMBERITAHUAN
    // DataPemberitahuanBinding().dependencies();
  }
}
