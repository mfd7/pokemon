import 'package:pokemon/domain/pokemon/entities/pokemon_detail.dart';

class PokemonDetailModel {
  PokemonDetailModel({
    List<Abilities>? abilities,
    num? baseExperience,
    Cries? cries,
    List<Forms>? forms,
    List<GameIndices>? gameIndices,
    num? height,
    List<HeldItems>? heldItems,
    num? id,
    bool? isDefault,
    String? locationAreaEncounters,
    List<Moves>? moves,
    String? name,
    num? order,
    Species? species,
    Sprites? sprites,
    List<Types>? types,
    num? weight,
  }) {
    _abilities = abilities;
    _baseExperience = baseExperience;
    _cries = cries;
    _forms = forms;
    _gameIndices = gameIndices;
    _height = height;
    _heldItems = heldItems;
    _id = id;
    _isDefault = isDefault;
    _locationAreaEncounters = locationAreaEncounters;
    _moves = moves;
    _name = name;
    _order = order;
    _species = species;
    _sprites = sprites;
    _types = types;
    _weight = weight;
  }

  PokemonDetailModel.fromJson(dynamic json) {
    if (json['abilities'] != null) {
      _abilities = [];
      json['abilities'].forEach((v) {
        _abilities?.add(Abilities.fromJson(v));
      });
    }
    _baseExperience = json['base_experience'];
    _cries = json['cries'] != null ? Cries.fromJson(json['cries']) : null;
    if (json['forms'] != null) {
      _forms = [];
      json['forms'].forEach((v) {
        _forms?.add(Forms.fromJson(v));
      });
    }
    if (json['game_indices'] != null) {
      _gameIndices = [];
      json['game_indices'].forEach((v) {
        _gameIndices?.add(GameIndices.fromJson(v));
      });
    }
    _height = json['height'];
    if (json['held_items'] != null) {
      _heldItems = [];
      json['held_items'].forEach((v) {
        _heldItems?.add(HeldItems.fromJson(v));
      });
    }
    _id = json['id'];
    _isDefault = json['is_default'];
    _locationAreaEncounters = json['location_area_encounters'];
    if (json['moves'] != null) {
      _moves = [];
      json['moves'].forEach((v) {
        _moves?.add(Moves.fromJson(v));
      });
    }
    _name = json['name'];
    _order = json['order'];
    _species =
        json['species'] != null ? Species.fromJson(json['species']) : null;
    _sprites =
        json['sprites'] != null ? Sprites.fromJson(json['sprites']) : null;
    if (json['types'] != null) {
      _types = [];
      json['types'].forEach((v) {
        _types?.add(Types.fromJson(v));
      });
    }
    _weight = json['weight'];
  }
  List<Abilities>? _abilities;
  num? _baseExperience;
  Cries? _cries;
  List<Forms>? _forms;
  List<GameIndices>? _gameIndices;
  num? _height;
  List<HeldItems>? _heldItems;
  num? _id;
  bool? _isDefault;
  String? _locationAreaEncounters;
  List<Moves>? _moves;
  String? _name;
  num? _order;
  Species? _species;
  Sprites? _sprites;
  List<Types>? _types;
  num? _weight;
  PokemonDetailModel copyWith({
    List<Abilities>? abilities,
    num? baseExperience,
    Cries? cries,
    List<Forms>? forms,
    List<GameIndices>? gameIndices,
    num? height,
    List<HeldItems>? heldItems,
    num? id,
    bool? isDefault,
    String? locationAreaEncounters,
    List<Moves>? moves,
    String? name,
    num? order,
    Species? species,
    Sprites? sprites,
    List<Types>? types,
    num? weight,
  }) =>
      PokemonDetailModel(
        abilities: abilities ?? _abilities,
        baseExperience: baseExperience ?? _baseExperience,
        cries: cries ?? _cries,
        forms: forms ?? _forms,
        gameIndices: gameIndices ?? _gameIndices,
        height: height ?? _height,
        heldItems: heldItems ?? _heldItems,
        id: id ?? _id,
        isDefault: isDefault ?? _isDefault,
        locationAreaEncounters:
            locationAreaEncounters ?? _locationAreaEncounters,
        moves: moves ?? _moves,
        name: name ?? _name,
        order: order ?? _order,
        species: species ?? _species,
        sprites: sprites ?? _sprites,
        types: types ?? _types,
        weight: weight ?? _weight,
      );
  List<Abilities>? get abilities => _abilities;
  num? get baseExperience => _baseExperience;
  Cries? get cries => _cries;
  List<Forms>? get forms => _forms;
  List<GameIndices>? get gameIndices => _gameIndices;
  num? get height => _height;
  List<HeldItems>? get heldItems => _heldItems;
  num? get id => _id;
  bool? get isDefault => _isDefault;
  String? get locationAreaEncounters => _locationAreaEncounters;
  List<Moves>? get moves => _moves;
  String? get name => _name;
  num? get order => _order;
  Species? get species => _species;
  Sprites? get sprites => _sprites;
  List<Types>? get types => _types;
  num? get weight => _weight;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_abilities != null) {
      map['abilities'] = _abilities?.map((v) => v.toJson()).toList();
    }
    map['base_experience'] = _baseExperience;
    if (_cries != null) {
      map['cries'] = _cries?.toJson();
    }
    if (_forms != null) {
      map['forms'] = _forms?.map((v) => v.toJson()).toList();
    }
    if (_gameIndices != null) {
      map['game_indices'] = _gameIndices?.map((v) => v.toJson()).toList();
    }
    map['height'] = _height;
    if (_heldItems != null) {
      map['held_items'] = _heldItems?.map((v) => v.toJson()).toList();
    }
    map['id'] = _id;
    map['is_default'] = _isDefault;
    map['location_area_encounters'] = _locationAreaEncounters;
    if (_moves != null) {
      map['moves'] = _moves?.map((v) => v.toJson()).toList();
    }
    map['name'] = _name;
    map['order'] = _order;
    if (_species != null) {
      map['species'] = _species?.toJson();
    }
    if (_sprites != null) {
      map['sprites'] = _sprites?.toJson();
    }
    if (_types != null) {
      map['types'] = _types?.map((v) => v.toJson()).toList();
    }
    map['weight'] = _weight;
    return map;
  }

  PokemonDetail toEntity() {
    return PokemonDetail(
      name: name ?? '',
      imageUrl: sprites?.other?.officialartwork?.frontDefault ?? '',
      types: types!.map((e) => e.type?.name ?? '').toList(),
    );
  }
}

class Types {
  Types({
    num? slot,
    Type? type,
  }) {
    _slot = slot;
    _type = type;
  }

  Types.fromJson(dynamic json) {
    _slot = json['slot'];
    _type = json['type'] != null ? Type.fromJson(json['type']) : null;
  }
  num? _slot;
  Type? _type;
  Types copyWith({
    num? slot,
    Type? type,
  }) =>
      Types(
        slot: slot ?? _slot,
        type: type ?? _type,
      );
  num? get slot => _slot;
  Type? get type => _type;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['slot'] = _slot;
    if (_type != null) {
      map['type'] = _type?.toJson();
    }
    return map;
  }
}

class Type {
  Type({
    String? name,
    String? url,
  }) {
    _name = name;
    _url = url;
  }

  Type.fromJson(dynamic json) {
    _name = json['name'];
    _url = json['url'];
  }
  String? _name;
  String? _url;
  Type copyWith({
    String? name,
    String? url,
  }) =>
      Type(
        name: name ?? _name,
        url: url ?? _url,
      );
  String? get name => _name;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['url'] = _url;
    return map;
  }
}

class Sprites {
  Sprites({
    String? backDefault,
    dynamic backFemale,
    String? backShiny,
    dynamic backShinyFemale,
    String? frontDefault,
    dynamic frontFemale,
    String? frontShiny,
    dynamic frontShinyFemale,
    Other? other,
  }) {
    _backDefault = backDefault;
    _backFemale = backFemale;
    _backShiny = backShiny;
    _backShinyFemale = backShinyFemale;
    _frontDefault = frontDefault;
    _frontFemale = frontFemale;
    _frontShiny = frontShiny;
    _frontShinyFemale = frontShinyFemale;
    _other = other;
  }

  Sprites.fromJson(dynamic json) {
    _backDefault = json['back_default'];
    _backFemale = json['back_female'];
    _backShiny = json['back_shiny'];
    _backShinyFemale = json['back_shiny_female'];
    _frontDefault = json['front_default'];
    _frontFemale = json['front_female'];
    _frontShiny = json['front_shiny'];
    _frontShinyFemale = json['front_shiny_female'];
    _other = json['other'] != null ? Other.fromJson(json['other']) : null;
  }
  String? _backDefault;
  dynamic _backFemale;
  String? _backShiny;
  dynamic _backShinyFemale;
  String? _frontDefault;
  dynamic _frontFemale;
  String? _frontShiny;
  dynamic _frontShinyFemale;
  Other? _other;
  Sprites copyWith({
    String? backDefault,
    dynamic backFemale,
    String? backShiny,
    dynamic backShinyFemale,
    String? frontDefault,
    dynamic frontFemale,
    String? frontShiny,
    dynamic frontShinyFemale,
    Other? other,
  }) =>
      Sprites(
        backDefault: backDefault ?? _backDefault,
        backFemale: backFemale ?? _backFemale,
        backShiny: backShiny ?? _backShiny,
        backShinyFemale: backShinyFemale ?? _backShinyFemale,
        frontDefault: frontDefault ?? _frontDefault,
        frontFemale: frontFemale ?? _frontFemale,
        frontShiny: frontShiny ?? _frontShiny,
        frontShinyFemale: frontShinyFemale ?? _frontShinyFemale,
        other: other ?? _other,
      );
  String? get backDefault => _backDefault;
  dynamic get backFemale => _backFemale;
  String? get backShiny => _backShiny;
  dynamic get backShinyFemale => _backShinyFemale;
  String? get frontDefault => _frontDefault;
  dynamic get frontFemale => _frontFemale;
  String? get frontShiny => _frontShiny;
  dynamic get frontShinyFemale => _frontShinyFemale;
  Other? get other => _other;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['back_default'] = _backDefault;
    map['back_female'] = _backFemale;
    map['back_shiny'] = _backShiny;
    map['back_shiny_female'] = _backShinyFemale;
    map['front_default'] = _frontDefault;
    map['front_female'] = _frontFemale;
    map['front_shiny'] = _frontShiny;
    map['front_shiny_female'] = _frontShinyFemale;
    if (_other != null) {
      map['other'] = _other?.toJson();
    }
    return map;
  }
}

class Other {
  Other({
    DreamWorld? dreamWorld,
    Home? home,
    OfficialArtwork? officialartwork,
    Showdown? showdown,
  }) {
    _dreamWorld = dreamWorld;
    _home = home;
    _officialartwork = officialartwork;
    _showdown = showdown;
  }

  Other.fromJson(dynamic json) {
    _dreamWorld = json['dream_world'] != null
        ? DreamWorld.fromJson(json['dream_world'])
        : null;
    _home = json['home'] != null ? Home.fromJson(json['home']) : null;
    _officialartwork = json['official-artwork'] != null
        ? OfficialArtwork.fromJson(json['official-artwork'])
        : null;
    _showdown =
        json['showdown'] != null ? Showdown.fromJson(json['showdown']) : null;
  }
  DreamWorld? _dreamWorld;
  Home? _home;
  OfficialArtwork? _officialartwork;
  Showdown? _showdown;
  Other copyWith({
    DreamWorld? dreamWorld,
    Home? home,
    OfficialArtwork? officialartwork,
    Showdown? showdown,
  }) =>
      Other(
        dreamWorld: dreamWorld ?? _dreamWorld,
        home: home ?? _home,
        officialartwork: officialartwork ?? _officialartwork,
        showdown: showdown ?? _showdown,
      );
  DreamWorld? get dreamWorld => _dreamWorld;
  Home? get home => _home;
  OfficialArtwork? get officialartwork => _officialartwork;
  Showdown? get showdown => _showdown;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_dreamWorld != null) {
      map['dream_world'] = _dreamWorld?.toJson();
    }
    if (_home != null) {
      map['home'] = _home?.toJson();
    }
    if (_officialartwork != null) {
      map['official-artwork'] = _officialartwork?.toJson();
    }
    if (_showdown != null) {
      map['showdown'] = _showdown?.toJson();
    }
    return map;
  }
}

class Showdown {
  Showdown({
    String? backDefault,
    dynamic backFemale,
    String? backShiny,
    dynamic backShinyFemale,
    String? frontDefault,
    dynamic frontFemale,
    String? frontShiny,
    dynamic frontShinyFemale,
  }) {
    _backDefault = backDefault;
    _backFemale = backFemale;
    _backShiny = backShiny;
    _backShinyFemale = backShinyFemale;
    _frontDefault = frontDefault;
    _frontFemale = frontFemale;
    _frontShiny = frontShiny;
    _frontShinyFemale = frontShinyFemale;
  }

  Showdown.fromJson(dynamic json) {
    _backDefault = json['back_default'];
    _backFemale = json['back_female'];
    _backShiny = json['back_shiny'];
    _backShinyFemale = json['back_shiny_female'];
    _frontDefault = json['front_default'];
    _frontFemale = json['front_female'];
    _frontShiny = json['front_shiny'];
    _frontShinyFemale = json['front_shiny_female'];
  }
  String? _backDefault;
  dynamic _backFemale;
  String? _backShiny;
  dynamic _backShinyFemale;
  String? _frontDefault;
  dynamic _frontFemale;
  String? _frontShiny;
  dynamic _frontShinyFemale;
  Showdown copyWith({
    String? backDefault,
    dynamic backFemale,
    String? backShiny,
    dynamic backShinyFemale,
    String? frontDefault,
    dynamic frontFemale,
    String? frontShiny,
    dynamic frontShinyFemale,
  }) =>
      Showdown(
        backDefault: backDefault ?? _backDefault,
        backFemale: backFemale ?? _backFemale,
        backShiny: backShiny ?? _backShiny,
        backShinyFemale: backShinyFemale ?? _backShinyFemale,
        frontDefault: frontDefault ?? _frontDefault,
        frontFemale: frontFemale ?? _frontFemale,
        frontShiny: frontShiny ?? _frontShiny,
        frontShinyFemale: frontShinyFemale ?? _frontShinyFemale,
      );
  String? get backDefault => _backDefault;
  dynamic get backFemale => _backFemale;
  String? get backShiny => _backShiny;
  dynamic get backShinyFemale => _backShinyFemale;
  String? get frontDefault => _frontDefault;
  dynamic get frontFemale => _frontFemale;
  String? get frontShiny => _frontShiny;
  dynamic get frontShinyFemale => _frontShinyFemale;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['back_default'] = _backDefault;
    map['back_female'] = _backFemale;
    map['back_shiny'] = _backShiny;
    map['back_shiny_female'] = _backShinyFemale;
    map['front_default'] = _frontDefault;
    map['front_female'] = _frontFemale;
    map['front_shiny'] = _frontShiny;
    map['front_shiny_female'] = _frontShinyFemale;
    return map;
  }
}

class OfficialArtwork {
  OfficialArtwork({
    String? frontDefault,
    String? frontShiny,
  }) {
    _frontDefault = frontDefault;
    _frontShiny = frontShiny;
  }

  OfficialArtwork.fromJson(dynamic json) {
    _frontDefault = json['front_default'];
    _frontShiny = json['front_shiny'];
  }
  String? _frontDefault;
  String? _frontShiny;
  OfficialArtwork copyWith({
    String? frontDefault,
    String? frontShiny,
  }) =>
      OfficialArtwork(
        frontDefault: frontDefault ?? _frontDefault,
        frontShiny: frontShiny ?? _frontShiny,
      );
  String? get frontDefault => _frontDefault;
  String? get frontShiny => _frontShiny;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['front_default'] = _frontDefault;
    map['front_shiny'] = _frontShiny;
    return map;
  }
}

class Home {
  Home({
    String? frontDefault,
    dynamic frontFemale,
    String? frontShiny,
    dynamic frontShinyFemale,
  }) {
    _frontDefault = frontDefault;
    _frontFemale = frontFemale;
    _frontShiny = frontShiny;
    _frontShinyFemale = frontShinyFemale;
  }

  Home.fromJson(dynamic json) {
    _frontDefault = json['front_default'];
    _frontFemale = json['front_female'];
    _frontShiny = json['front_shiny'];
    _frontShinyFemale = json['front_shiny_female'];
  }
  String? _frontDefault;
  dynamic _frontFemale;
  String? _frontShiny;
  dynamic _frontShinyFemale;
  Home copyWith({
    String? frontDefault,
    dynamic frontFemale,
    String? frontShiny,
    dynamic frontShinyFemale,
  }) =>
      Home(
        frontDefault: frontDefault ?? _frontDefault,
        frontFemale: frontFemale ?? _frontFemale,
        frontShiny: frontShiny ?? _frontShiny,
        frontShinyFemale: frontShinyFemale ?? _frontShinyFemale,
      );
  String? get frontDefault => _frontDefault;
  dynamic get frontFemale => _frontFemale;
  String? get frontShiny => _frontShiny;
  dynamic get frontShinyFemale => _frontShinyFemale;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['front_default'] = _frontDefault;
    map['front_female'] = _frontFemale;
    map['front_shiny'] = _frontShiny;
    map['front_shiny_female'] = _frontShinyFemale;
    return map;
  }
}

class DreamWorld {
  DreamWorld({
    String? frontDefault,
    dynamic frontFemale,
  }) {
    _frontDefault = frontDefault;
    _frontFemale = frontFemale;
  }

  DreamWorld.fromJson(dynamic json) {
    _frontDefault = json['front_default'];
    _frontFemale = json['front_female'];
  }
  String? _frontDefault;
  dynamic _frontFemale;
  DreamWorld copyWith({
    String? frontDefault,
    dynamic frontFemale,
  }) =>
      DreamWorld(
        frontDefault: frontDefault ?? _frontDefault,
        frontFemale: frontFemale ?? _frontFemale,
      );
  String? get frontDefault => _frontDefault;
  dynamic get frontFemale => _frontFemale;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['front_default'] = _frontDefault;
    map['front_female'] = _frontFemale;
    return map;
  }
}

class Species {
  Species({
    String? name,
    String? url,
  }) {
    _name = name;
    _url = url;
  }

  Species.fromJson(dynamic json) {
    _name = json['name'];
    _url = json['url'];
  }
  String? _name;
  String? _url;
  Species copyWith({
    String? name,
    String? url,
  }) =>
      Species(
        name: name ?? _name,
        url: url ?? _url,
      );
  String? get name => _name;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['url'] = _url;
    return map;
  }
}

class Moves {
  Moves({
    Move? move,
    List<VersionGroupDetails>? versionGroupDetails,
  }) {
    _move = move;
    _versionGroupDetails = versionGroupDetails;
  }

  Moves.fromJson(dynamic json) {
    _move = json['move'] != null ? Move.fromJson(json['move']) : null;
    if (json['version_group_details'] != null) {
      _versionGroupDetails = [];
      json['version_group_details'].forEach((v) {
        _versionGroupDetails?.add(VersionGroupDetails.fromJson(v));
      });
    }
  }
  Move? _move;
  List<VersionGroupDetails>? _versionGroupDetails;
  Moves copyWith({
    Move? move,
    List<VersionGroupDetails>? versionGroupDetails,
  }) =>
      Moves(
        move: move ?? _move,
        versionGroupDetails: versionGroupDetails ?? _versionGroupDetails,
      );
  Move? get move => _move;
  List<VersionGroupDetails>? get versionGroupDetails => _versionGroupDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_move != null) {
      map['move'] = _move?.toJson();
    }
    if (_versionGroupDetails != null) {
      map['version_group_details'] =
          _versionGroupDetails?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class VersionGroupDetails {
  VersionGroupDetails({
    num? levelLearnedAt,
    MoveLearnMethod? moveLearnMethod,
    VersionGroup? versionGroup,
  }) {
    _levelLearnedAt = levelLearnedAt;
    _moveLearnMethod = moveLearnMethod;
    _versionGroup = versionGroup;
  }

  VersionGroupDetails.fromJson(dynamic json) {
    _levelLearnedAt = json['level_learned_at'];
    _moveLearnMethod = json['move_learn_method'] != null
        ? MoveLearnMethod.fromJson(json['move_learn_method'])
        : null;
    _versionGroup = json['version_group'] != null
        ? VersionGroup.fromJson(json['version_group'])
        : null;
  }
  num? _levelLearnedAt;
  MoveLearnMethod? _moveLearnMethod;
  VersionGroup? _versionGroup;
  VersionGroupDetails copyWith({
    num? levelLearnedAt,
    MoveLearnMethod? moveLearnMethod,
    VersionGroup? versionGroup,
  }) =>
      VersionGroupDetails(
        levelLearnedAt: levelLearnedAt ?? _levelLearnedAt,
        moveLearnMethod: moveLearnMethod ?? _moveLearnMethod,
        versionGroup: versionGroup ?? _versionGroup,
      );
  num? get levelLearnedAt => _levelLearnedAt;
  MoveLearnMethod? get moveLearnMethod => _moveLearnMethod;
  VersionGroup? get versionGroup => _versionGroup;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['level_learned_at'] = _levelLearnedAt;
    if (_moveLearnMethod != null) {
      map['move_learn_method'] = _moveLearnMethod?.toJson();
    }
    if (_versionGroup != null) {
      map['version_group'] = _versionGroup?.toJson();
    }
    return map;
  }
}

class VersionGroup {
  VersionGroup({
    String? name,
    String? url,
  }) {
    _name = name;
    _url = url;
  }

  VersionGroup.fromJson(dynamic json) {
    _name = json['name'];
    _url = json['url'];
  }
  String? _name;
  String? _url;
  VersionGroup copyWith({
    String? name,
    String? url,
  }) =>
      VersionGroup(
        name: name ?? _name,
        url: url ?? _url,
      );
  String? get name => _name;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['url'] = _url;
    return map;
  }
}

class MoveLearnMethod {
  MoveLearnMethod({
    String? name,
    String? url,
  }) {
    _name = name;
    _url = url;
  }

  MoveLearnMethod.fromJson(dynamic json) {
    _name = json['name'];
    _url = json['url'];
  }
  String? _name;
  String? _url;
  MoveLearnMethod copyWith({
    String? name,
    String? url,
  }) =>
      MoveLearnMethod(
        name: name ?? _name,
        url: url ?? _url,
      );
  String? get name => _name;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['url'] = _url;
    return map;
  }
}

class Move {
  Move({
    String? name,
    String? url,
  }) {
    _name = name;
    _url = url;
  }

  Move.fromJson(dynamic json) {
    _name = json['name'];
    _url = json['url'];
  }
  String? _name;
  String? _url;
  Move copyWith({
    String? name,
    String? url,
  }) =>
      Move(
        name: name ?? _name,
        url: url ?? _url,
      );
  String? get name => _name;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['url'] = _url;
    return map;
  }
}

class HeldItems {
  HeldItems({
    Item? item,
    List<VersionDetails>? versionDetails,
  }) {
    _item = item;
    _versionDetails = versionDetails;
  }

  HeldItems.fromJson(dynamic json) {
    _item = json['item'] != null ? Item.fromJson(json['item']) : null;
    if (json['version_details'] != null) {
      _versionDetails = [];
      json['version_details'].forEach((v) {
        _versionDetails?.add(VersionDetails.fromJson(v));
      });
    }
  }
  Item? _item;
  List<VersionDetails>? _versionDetails;
  HeldItems copyWith({
    Item? item,
    List<VersionDetails>? versionDetails,
  }) =>
      HeldItems(
        item: item ?? _item,
        versionDetails: versionDetails ?? _versionDetails,
      );
  Item? get item => _item;
  List<VersionDetails>? get versionDetails => _versionDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_item != null) {
      map['item'] = _item?.toJson();
    }
    if (_versionDetails != null) {
      map['version_details'] = _versionDetails?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class VersionDetails {
  VersionDetails({
    num? rarity,
    Version? version,
  }) {
    _rarity = rarity;
    _version = version;
  }

  VersionDetails.fromJson(dynamic json) {
    _rarity = json['rarity'];
    _version =
        json['version'] != null ? Version.fromJson(json['version']) : null;
  }
  num? _rarity;
  Version? _version;
  VersionDetails copyWith({
    num? rarity,
    Version? version,
  }) =>
      VersionDetails(
        rarity: rarity ?? _rarity,
        version: version ?? _version,
      );
  num? get rarity => _rarity;
  Version? get version => _version;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['rarity'] = _rarity;
    if (_version != null) {
      map['version'] = _version?.toJson();
    }
    return map;
  }
}

class Version {
  Version({
    String? name,
    String? url,
  }) {
    _name = name;
    _url = url;
  }

  Version.fromJson(dynamic json) {
    _name = json['name'];
    _url = json['url'];
  }
  String? _name;
  String? _url;
  Version copyWith({
    String? name,
    String? url,
  }) =>
      Version(
        name: name ?? _name,
        url: url ?? _url,
      );
  String? get name => _name;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['url'] = _url;
    return map;
  }
}

class Item {
  Item({
    String? name,
    String? url,
  }) {
    _name = name;
    _url = url;
  }

  Item.fromJson(dynamic json) {
    _name = json['name'];
    _url = json['url'];
  }
  String? _name;
  String? _url;
  Item copyWith({
    String? name,
    String? url,
  }) =>
      Item(
        name: name ?? _name,
        url: url ?? _url,
      );
  String? get name => _name;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['url'] = _url;
    return map;
  }
}

class GameIndices {
  GameIndices({
    num? gameIndex,
    Version? version,
  }) {
    _gameIndex = gameIndex;
    _version = version;
  }

  GameIndices.fromJson(dynamic json) {
    _gameIndex = json['game_index'];
    _version =
        json['version'] != null ? Version.fromJson(json['version']) : null;
  }
  num? _gameIndex;
  Version? _version;
  GameIndices copyWith({
    num? gameIndex,
    Version? version,
  }) =>
      GameIndices(
        gameIndex: gameIndex ?? _gameIndex,
        version: version ?? _version,
      );
  num? get gameIndex => _gameIndex;
  Version? get version => _version;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['game_index'] = _gameIndex;
    if (_version != null) {
      map['version'] = _version?.toJson();
    }
    return map;
  }
}

class Forms {
  Forms({
    String? name,
    String? url,
  }) {
    _name = name;
    _url = url;
  }

  Forms.fromJson(dynamic json) {
    _name = json['name'];
    _url = json['url'];
  }
  String? _name;
  String? _url;
  Forms copyWith({
    String? name,
    String? url,
  }) =>
      Forms(
        name: name ?? _name,
        url: url ?? _url,
      );
  String? get name => _name;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['url'] = _url;
    return map;
  }
}

class Cries {
  Cries({
    String? latest,
    String? legacy,
  }) {
    _latest = latest;
    _legacy = legacy;
  }

  Cries.fromJson(dynamic json) {
    _latest = json['latest'];
    _legacy = json['legacy'];
  }
  String? _latest;
  String? _legacy;
  Cries copyWith({
    String? latest,
    String? legacy,
  }) =>
      Cries(
        latest: latest ?? _latest,
        legacy: legacy ?? _legacy,
      );
  String? get latest => _latest;
  String? get legacy => _legacy;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['latest'] = _latest;
    map['legacy'] = _legacy;
    return map;
  }
}

class Abilities {
  Abilities({
    Ability? ability,
    bool? isHidden,
    num? slot,
  }) {
    _ability = ability;
    _isHidden = isHidden;
    _slot = slot;
  }

  Abilities.fromJson(dynamic json) {
    _ability =
        json['ability'] != null ? Ability.fromJson(json['ability']) : null;
    _isHidden = json['is_hidden'];
    _slot = json['slot'];
  }
  Ability? _ability;
  bool? _isHidden;
  num? _slot;
  Abilities copyWith({
    Ability? ability,
    bool? isHidden,
    num? slot,
  }) =>
      Abilities(
        ability: ability ?? _ability,
        isHidden: isHidden ?? _isHidden,
        slot: slot ?? _slot,
      );
  Ability? get ability => _ability;
  bool? get isHidden => _isHidden;
  num? get slot => _slot;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_ability != null) {
      map['ability'] = _ability?.toJson();
    }
    map['is_hidden'] = _isHidden;
    map['slot'] = _slot;
    return map;
  }
}

class Ability {
  Ability({
    String? name,
    String? url,
  }) {
    _name = name;
    _url = url;
  }

  Ability.fromJson(dynamic json) {
    _name = json['name'];
    _url = json['url'];
  }
  String? _name;
  String? _url;
  Ability copyWith({
    String? name,
    String? url,
  }) =>
      Ability(
        name: name ?? _name,
        url: url ?? _url,
      );
  String? get name => _name;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['url'] = _url;
    return map;
  }
}
