import 'package:pokemon/domain/pokemon/entities/pokemon.dart';
import 'package:pokemon/domain/pokemon/entities/pokemon_list.dart';

class PokemonListModel {
  PokemonListModel({
    num? count,
    String? next,
    String? previous,
    List<PokemonModel>? results,
  }) {
    _count = count;
    _next = next;
    _previous = previous;
    _results = results;
  }

  PokemonListModel.fromJson(dynamic json) {
    _count = json['count'];
    _next = json['next'];
    _previous = json['previous'];
    if (json['results'] != null) {
      _results = [];
      json['results'].forEach((v) {
        _results?.add(PokemonModel.fromJson(v));
      });
    }
  }
  num? _count;
  String? _next;
  String? _previous;
  List<PokemonModel>? _results;
  PokemonListModel copyWith({
    num? count,
    String? next,
    String? previous,
    List<PokemonModel>? results,
  }) =>
      PokemonListModel(
        count: count ?? _count,
        next: next ?? _next,
        previous: previous ?? _previous,
        results: results ?? _results,
      );
  num get count => _count ?? 0;
  String get next => _next ?? '';
  String get previous => _previous ?? '';
  List<PokemonModel> get results => _results ?? [];

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    map['next'] = _next;
    map['previous'] = _previous;
    if (_results != null) {
      map['results'] = _results?.map((v) => v.toJson()).toList();
    }
    return map;
  }

  PokemonList toEntity() {
    return PokemonList(
        next: next, pokemonList: results.map((e) => e.toEntity()).toList());
  }
}

class PokemonModel {
  PokemonModel({
    String? name,
    String? url,
  }) {
    _name = name;
    _url = url;
  }

  PokemonModel.fromJson(dynamic json) {
    _name = json['name'];
    _url = json['url'];
  }
  String? _name;
  String? _url;
  PokemonModel copyWith({
    String? name,
    String? url,
  }) =>
      PokemonModel(
        name: name ?? _name,
        url: url ?? _url,
      );
  String get name => _name ?? '';
  String get url => _url ?? '';

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['url'] = _url;
    return map;
  }

  Pokemon toEntity() {
    return Pokemon(
      name: name,
      detailLink: url,
    );
  }
}
