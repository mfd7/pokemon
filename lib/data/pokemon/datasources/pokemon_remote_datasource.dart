import 'package:pokemon/_core/utils/logger.dart';
import 'package:pokemon/data/_core/api_base_helper.dart';
import 'package:pokemon/data/pokemon/models/pokemon_detail_model.dart';
import 'package:pokemon/data/pokemon/models/pokemon_list_model.dart';
import 'package:pokemon/data/pokemon/pokemon_endpoints.dart';

abstract class PokemonRemoteDatasource {
  Future<PokemonListModel> retrievePokemonList(int offset);
  Future<PokemonDetailModel> retrievePokemonDetail(String id);
}

class PokemonRemoteDatasourceImpl implements PokemonRemoteDatasource {
  final ApiBaseHelper apiBaseHelper;

  PokemonRemoteDatasourceImpl(this.apiBaseHelper);

  @override
  Future<PokemonListModel> retrievePokemonList(int offset) async {
    final responseBody = await apiBaseHelper.getApi(
        PokemonEndpoints.pokemonList,
        query: {'offset': offset.toString(), 'limit': '10'});
    final model = PokemonListModel.fromJson(responseBody);
    return model;
  }

  @override
  Future<PokemonDetailModel> retrievePokemonDetail(String id) async {
    final responseBody =
        await apiBaseHelper.getApi('${PokemonEndpoints.pokemonDetail}/$id');
    final model = PokemonDetailModel.fromJson(responseBody);
    return model;
  }
}
