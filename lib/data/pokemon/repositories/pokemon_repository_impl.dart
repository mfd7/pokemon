import 'package:dartz/dartz.dart';
import 'package:pokemon/_core/extensions.dart';
import 'package:pokemon/data/_core/app_exceptions.dart';
import 'package:pokemon/data/pokemon/datasources/pokemon_remote_datasource.dart';
import 'package:pokemon/domain/_core/app_failures.dart';
import 'package:pokemon/domain/pokemon/entities/pokemon_detail.dart';
import 'package:pokemon/domain/pokemon/entities/pokemon_list.dart';
import 'package:pokemon/domain/pokemon/repositories/pokemon_repository.dart';

class PokemonRepositoryImpl implements PokemonRepository {
  final PokemonRemoteDatasource pokemonRemoteDatasource;

  PokemonRepositoryImpl(this.pokemonRemoteDatasource);

  @override
  Future<Either<AppFailure, List<PokemonDetail>>> retrievePokemonList(
      int offset) async {
    try {
      final pokemon = await pokemonRemoteDatasource.retrievePokemonList(offset);
      final pokemonEntity = pokemon.toEntity();
      List<PokemonDetail> pokemonDetailList = [];
      for (var detail in pokemonEntity.pokemonList) {
        final retrieveDetail =
            await pokemonRemoteDatasource.retrievePokemonDetail(detail.name);
        pokemonDetailList.add(retrieveDetail.toEntity());
      }
      return Right(pokemonDetailList);
    } on AppException catch (e) {
      return Left(e.toAppFailure());
    } catch (e) {
      return const Left(AppFailure.general("Unknown Error"));
    }
  }

  @override
  Future<Either<AppFailure, PokemonDetail>> retrievePokemonDetail(
      String id) async {
    try {
      final pokemon = await pokemonRemoteDatasource.retrievePokemonDetail(id);
      return Right(pokemon.toEntity());
    } on AppException catch (e) {
      return Left(e.toAppFailure());
    } catch (e) {
      return const Left(AppFailure.general("Unknown Error"));
    }
  }
}
