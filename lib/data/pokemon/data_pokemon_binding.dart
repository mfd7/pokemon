import 'package:get/get.dart';
import 'package:pokemon/data/pokemon/datasources/pokemon_remote_datasource.dart';
import 'package:pokemon/data/pokemon/repositories/pokemon_repository_impl.dart';
import 'package:pokemon/domain/pokemon/repositories/pokemon_repository.dart';

class DataPokemonBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PokemonRemoteDatasource>(
        () => PokemonRemoteDatasourceImpl(Get.find()),
        fenix: true);

    Get.lazyPut<PokemonRepository>(() => PokemonRepositoryImpl(Get.find()),
        fenix: true);
  }
}
